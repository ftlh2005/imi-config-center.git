<?php

declare(strict_types=1);

namespace Phpben\Imi\ConfigCenter\Driver;

use RuntimeException;
use Imi\Config;

/**
 * 本地文件驱动
 */
class File
{
    /**
     * 获取配置
     * @return array
     */
    public function get(): array
    {
        $website_file = Config::get('@app.configs.website');
        if (!is_file($website_file)) {
            throw new RuntimeException('config file:/config/website.php is not found');
        }
        return (include $website_file);
    }

    /**
     * 更新配置
     * @param array $content 配置内容
     */
    public function pull(array $content): void
    {
        $website_file = Config::get('@app.configs.website');
        if (!is_file($website_file)) {
            throw new RuntimeException('config file:/config/website.php is not found');
        }
        $content = "<?php \n\nreturn " . var_export($content, true) . ";\n";
        file_put_contents($website_file, $content);
    }

}